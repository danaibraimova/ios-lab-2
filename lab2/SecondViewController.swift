//
//  SecondViewController.swift
//  lab2
//
//  Created by user on 16.09.17.
//  Copyright © 2017 KBTU. All rights reserved.
//

//import Foundation
import UIKit

class SecondViewController: UIViewController {
    
    @IBOutlet weak var questionLabel: UILabel!
    

    var dictionary = ["Simpsons or Griffins?":"Simpsons", "The Beatles or The Rolling Stones?":"The Beatles", "Green or orange?":"Orange", "C# or Java?":"C#", "KBTU or Politeh?":"KBTU", "Coffee or tea?":"Coffee"]
  
    var questions = ["Simpsons or Griffins?", "The Beatles or The Rolling Stones?",  "Green or orange?", "C# or Java?", "KBTU or Politeh?", "Coffee or tea?"]
    
    var answers: [[String]] = [
        ["Simpsons", "Griffins"],
        ["The Beatles", "The Rolling Stones"],
        ["Green", "Orange"],
        ["C#", "Java"],
        ["KBTU", "Politeh"],
        ["Coffee", "Tea"],
        ]

    
    var cur: Int = 0
    var totalScore: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NewQuestion()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func NewQuestion() {
        //questionLabel.text = Array(dictionary.keys)[cur]
        questionLabel.text = questions[cur]
        let variant = answers[cur]
        for i in 0..<variant.count
        {
            let button = UIButton(frame: CGRect(x: 16, y: 50*i+150, width: Int(UIScreen.main.bounds.size.width-32), height: 45))
            button.backgroundColor = UIColor.lightGray
            button.titleLabel!.font =  UIFont(name: "Avenir Book", size: 17)
            button.setTitle(variant[i], for: UIControlState.normal)
            button.addTarget(self, action:#selector(buttonClicked(_:)), for: .touchUpInside)
            
            self.view.addSubview(button)
        }
    }
    
    
    func buttonClicked(_ sender:UIButton) {
        
        //if(sender.currentTitle == dictionary.values[cur]) {
        if(sender.currentTitle! == dictionary[questionLabel.text!]) {
            totalScore+=1
            super.loadView()
            self.view.backgroundColor = #colorLiteral(red: 0.5915235718, green: 0.7647058964, blue: 0.7219469397, alpha: 1)
        }
        else
        {
            super.loadView()
            self.view.backgroundColor = #colorLiteral(red: 0.7647058964, green: 0.4293644638, blue: 0.5062804725, alpha: 1)
        }
        if cur < dictionary.count-1 {
            cur+=1
            NewQuestion()
        }
        else
        {
            super.loadView()
            questionLabel.text = String(totalScore) + " from " + String(dictionary.count)
            let button = UIButton(frame: CGRect(x: 16, y: 150, width: Int(UIScreen.main.bounds.size.width-32), height: 45))
            button.backgroundColor = UIColor.darkGray
            button.setTitle("Restart", for: UIControlState.normal)
            button.addTarget(self, action:#selector(restart(_:)), for: .touchUpInside)
            self.view.addSubview(button)
        }
    }
    
    func restart(_ sender:UIButton) {
        cur = 0
        totalScore = 0
        super.loadView()
        NewQuestion()
    }
    
    
    

    
}
